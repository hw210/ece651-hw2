package hw1;

import java.util.List;

public class BlueDevil extends person {
	private String major;
	private String degree;
	private String enrollYear;
	private String GPA;
	private String role;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public void setDuke(String[] data) {
		this.setMajor(data[0]);
		this.setDegree(data[2]);
		this.setEnrollYear(data[1]);
		this.setGPA(data[3]);
		this.setRole(data[4]);
	} 
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getEnrollYear() {
		return enrollYear;
	}
	public void setEnrollYear(String enrollYear) {
		this.enrollYear = enrollYear;
	}
	public String getGPA() {
		return GPA;
	}
	public void setGPA(String gPA) {
		GPA = gPA;
	}
}
