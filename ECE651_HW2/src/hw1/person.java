package hw1;

import java.util.List;

public class person {
	private String name;
	private String gender;
	
	private String age;
	private String country;
	private String workExperience;
	private String hobbit;
	private String education;
	
	public void setPerson(String[] data) {
		this.setName(data[0]);
		this.setAge(data[1]);
		this.setCountry(data[2]);
		this.setHobbit(data[4]);
		this.setWorkExperience(data[3]);
		this.setGender(data[5]);
		this.setEducation(data[6]);
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getWorkExperience() {
		return workExperience;
	}
	public void setWorkExperience(String workExperience) {
		if (workExperience == null) {
			this.workExperience = "does not have work experience";
		}
		else {
			this.workExperience = workExperience;
		}
	}
	public String getHobbit() {
		return hobbit;
	}
	public void setHobbit(String hobbit) {
		if (hobbit == null) {
			this.hobbit = "stay at home";
		}
		else {
			this.hobbit = hobbit;
		}
		
	}
	
}

