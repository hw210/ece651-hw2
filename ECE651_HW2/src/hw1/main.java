package hw1;

import java.util.*;
import java.util.List;


public class main {
	public static void whoIs(String name) {//search by name
		for (person member:dataBase) {
			if (member.getName() == name) {
				PrintInfo(member);
				return;
			}	
		}
		System.out.println("Failed to find " + name + " !");
	}
	
	public static void howOldIs(String age) {//search by age
		boolean isExist = false;
		for (person member:dataBase) {
			if (member.getAge() == age) {
				PrintInfo(member);
				isExist = true;
				
			}
		}	
		if (!isExist) {
			System.out.println("Failed to find person at this age!");
		}
		
	}
	
	public static void  whatDegreeIs(String degree) {
		boolean isExist = false;
		for (person member:dataBase) {
			if (member instanceof BlueDevil) {
				if (((BlueDevil) member).getDegree() == degree) {
					PrintInfo(member);
					isExist = true;
				}
			}
			
		}
		if (!isExist) {
			System.out.println("Failed to finde person has this degree!");
		}
	}
	
	public static void PrintInfo(person member) {//print information about search result
		String gender;
		System.out.println(member.getName()+" is "+ member.getAge() +" years old from " +member.getCountry() + " and received undergraduate from " + member.getEducation());
		if (member.getGender() == "male") {
			gender = "he";
		}
		else {
			gender = "she";
		}
		System.out.println(gender +" " + member.getWorkExperience() + " ,and likes to " + member.getHobbit());
		
		if (member instanceof BlueDevil ) {
		System.out.println(gender + " is one " + ((BlueDevil) member).getMajor() + " " + ((BlueDevil) member).getDegree() + " " + ((BlueDevil) member).getRole());
		System.out.println(gender + " enrolled in " + ((BlueDevil) member).getEnrollYear() + " and has GPA " + ((BlueDevil) member).getGPA());
		}
		System.out.println(" ");
	}
	public static void InfoAdd(String[] personInfo,String[] DukeInfo) {//add person object to dataBase list
		if (DukeInfo == null) {
			person member = new person();
			member.setPerson(personInfo);
			dataBase.add(member);
		}
		else {
			BlueDevil member = new BlueDevil();
			member.setPerson(personInfo);
			member.setDuke(DukeInfo);
			dataBase.add(member);
		}
		
	
		
	}
	
	public static void  InfoDelete(String name) {//delete person object from dataBase list
		for (person member: dataBase) {
			if (member.getName() == name) {
				if (dataBase.remove(member)) {
					System.out.printf("Delete %s from dataBase!\n",name);
					return;
				}
				else {
					System.out.printf("Failed to delete %s from dataBase!\n",name);
					return;
				}
				
			}
			
		}
		System.out.printf("Failed to find %s from dataBase!\n",name);
	}
	
	public static List<person> dataBase = new ArrayList<person>();//the list of person objects.
	
	
	
	
	
	public static void main(String[] args) {
		//data input
		String[] Mario_person = {"Mario","24","China","worked in Cedars-Sinai as an intern",null,"male","Nintendo University"};
		String[] Mario_blue = {"ECE","2014","Phd","3.99","student"};
		String[] Haili_person = {"Haili","12","China",null,"work out","male","Beihang University"};
		String[] Yuanyuan_person = {"Yuanyuan","24","China",null,"play baseball and fencing","female","ECUST"};
		String[] Yuanyuan_blue = {"ECE","2017","Master","4.0","student"};
		String[] You_person = {"You","24","China",null,"travel and listen music","female","Duke University"};
		String[] You_blue = {"ECE","2017","Master","4.0","student"};
		String[] Lei_person = {"Lei","24","China","worked on research project about image resolution","enjoy climbing and animals","male","Nintendo University"};
		String[] Lei_blue = {"ECE","2017","Master","4.0","student"};
		String[] Shalin_person = {"Shalin","24","China",null,"do bodybuilding and dancing","female","DA-IICT"};
		String[] Shalin_blue = {"ECE","2017","Master","4.0","student"};
		String[] Ric_person = {"Ric","55","America","retired from IBM as Vice President","play golf and sand volleyball","male","Trinity University"};
		String[] Ric_blue = {"ECE","1987","Phd","4.0","professor"};
		
		//add data to database
		InfoAdd(Mario_person,Mario_blue);
		InfoAdd(Haili_person,null);
		InfoAdd(Yuanyuan_person,Yuanyuan_blue);
		InfoAdd(You_person,You_blue);
		InfoAdd(Lei_person,Lei_blue);
		InfoAdd(Shalin_person,Shalin_blue);
		InfoAdd(Ric_person,Ric_blue);
		
		//test search function:whoIs ,howOldIs and whatDegreeIs
		whoIs("Mario");
		whoIs("Naruto");
		whoIs("Haili");
		howOldIs("24");
		whatDegreeIs("Phd");
		
		// delete person object from dataBase
		InfoDelete("Ric");
		InfoDelete("Naruto");
		
		
		
	}

}
